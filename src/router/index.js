import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  // 设置一级路由
  { path: '/', component: () => import('@/views/WeatherSearch/index.vue') },
  { path: '/weather', component: () => import('@/views/WeatherSearch/index.vue') },
  { path: '*', component: () => import('@/views/NotFound.vue') },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
